<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\LogDataController;
use App\Http\Controllers\PersonController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', [UserController::class, 'store']);

Route::prefix('auth')->middleware(['api'])->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
});

Route::prefix('person')->group(function () {
    Route::name('index')->get('/', [PersonController::class, 'index']);
    Route::name('store')->post('/store', [PersonController::class, 'store']);
    Route::name('show')->get('/show/{id}', [PersonController::class, 'show']);
    Route::name('update')->put('/update/{id}', [PersonController::class, 'update']);
    Route::name('destroy')->delete('/destroy/{id}', [PersonController::class, 'destroy']);
});
Route::prefix('film')->group(function () {
    Route::name('index')->get('/', [FilmController::class, 'index']);
    Route::name('store')->post('/store', [FilmController::class, 'store']);
    Route::name('import')->post('/import', [FilmController::class, 'import']);
    Route::name('show')->get('/show/{id}', [FilmController::class, 'show']);
    Route::name('update')->put('/update/{id}', [FilmController::class, 'update']);
    Route::name('destroy')->delete('/destroy/{id}', [FilmController::class, 'destroy']);
    Route::name('validateForStore')->post('/validate', [FilmController::class, 'validateForStore']);
});
Route::prefix('language')->group(function () {
    Route::name('index')->get('/', [LanguageController::class, 'index']);
    Route::name('store')->post('/store', [LanguageController::class, 'store']);
    // Route::name('show')->get('/show/{id}', [LanguageController::class, 'show']);
    // Route::name('update')->put('/update/{id}', [LanguageController::class, 'update']);
    // Route::name('destroy')->delete('/destroy/{id}', [LanguageController::class, 'destroy']);
});
Route::prefix('country')->group(function () {
    Route::name('index')->get('/', [CountryController::class, 'index']);
    Route::name('store')->post('/store', [CountryController::class, 'store']);
    // Route::name('show')->get('/show/{id}', [CountryController::class, 'show']);
    // Route::name('update')->put('/update/{id}', [CountryController::class, 'update']);
    // Route::name('destroy')->delete('/destroy/{id}', [CountryController::class, 'destroy']);
});
Route::prefix('logData')->group(function () {
    Route::name('index')->get('/', [LogDataController::class, 'index']);
    // Route::name('store')->post('/store', [CountryController::class, 'store']);
    // Route::name('show')->get('/show/{id}', [CountryController::class, 'show']);
    // Route::name('update')->put('/update/{id}', [CountryController::class, 'update']);
    // Route::name('destroy')->delete('/destroy/{id}', [CountryController::class, 'destroy']);
});
// Route::get('/', [UsuarioController::class, 'index'])->name('obtener-usuarios');
