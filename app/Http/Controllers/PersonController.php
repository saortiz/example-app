<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;
use App\Http\Requests\PersonRequest;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // dd('Hola');
        $persons = Person::all();

        return($persons);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PersonRequest $request)
    {
        
        $person = new Person();

        $person->name = $request->name;
        $person->lastname = $request->lastname;
        $person->birthdate = $request->birthdate;
        $person->phone = $request->phone;
        $person->email = $request->email;
        $person->address = $request->address;


        $person->save();
        
        return $person;

    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $person = Person::find($id);
        return $person;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $person = Person::find($id);

        if($person != null){
            return response()->json(['msg'=>'Persona no encontrada']);
        }

        $person->name = $request->name;
        $person->lastname = $request->lastname;
        $person->birthdate = $request->birthdate;
        $person->phone = $request->phone;
        $person->email = $request->email;
        $person->address = $request->address;


        $person->save();

        return response()->json(['msg'=>'Datos de persona actualizados']);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $person = Person::find($id);
        
        if($person != null){
            return response()->json(['msg'=>'Persona no encontrada']);
        }
        $person->delete();
        return response()->json(['msg'=>'Persona eliminada']);
    }
}
