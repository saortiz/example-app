<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use App\Models\User;

class UserController extends Controller
{
    public function store(UserRequest $request)
    {
        $user = new User($request->input());
        $user->password = Hash::make($request->password, ['rounds' => 12]);
        $user->saveOrFail();
        return response()->json(['msg' => 'Usuario creado correctamente'], 200);
    }
}
