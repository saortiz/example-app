<?php

namespace App\Http\Controllers;

use App\Models\LogData;
use Illuminate\Http\Request;

class LogDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $logData = LogData::all();
        // dd($logData);
        return response()->json(['data' => $logData], 200);
    }

    /*
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(LogData $logData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(LogData $logData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, LogData $logData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(LogData $logData)
    {
        //
    }
}
