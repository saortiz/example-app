<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilmRequest;
use App\Imports\FilmsImport;
use App\Models\Film;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FilmController extends Controller
{
    public function index()
    {
        $films = Film::all();

        return($films);
    }

    public function store(FilmRequest $request)
    {
        $film = new Film($request->input());
        $film->saveOrFail();
        
        return $film;
    }

    public function import(Request $request) 
    {
        Excel::import(new FilmsImport, $request->films);
    
        return response()->json(['msg' => 'Archivo importado correctamente'], 200);
    }

    public function show(string $id)
    {
        $film = Film::find($id);

        return ($film);
    }

    public function update(Request $request, string $id)
    {
        $film = Film::find($id);

        if($film == null){
            return response()->json(['msg'=> 'Pelicula no encontrada']);
        }
        $film->title = $request->title;
        $film->director = $request->director;
        $film->launch = $request->launch;
        $film->gender = $request->gender;
        $film->duration = $request->duration;
        $film->synopsis = $request->synopsis;
        $film->actors = $request->actors;
        $film->country_id = $request->country_id;
        $film->classification = $request->classification;
        $film->score = $request->score;
        $film->production = $request->production;
        $film->format = $request->format;
        $film->language_id = $request->language_id;


        $film->save();

        return response()->json(['msg'=> 'Datos de pelicula actualizados']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $film = Film::find($id);
        if($film == null){
            return response()->json(['msg'=> 'Pelicula no encontrada']);
        }
        $film->delete();

        return response()->json(['msg'=> 'Pelicula eliminada'], 200);
    }

    public function validateForStore(FilmRequest $request){
        Film::create($request->all());

        return response()->json(['msg'=> 'Pelicula Agregada'], 200);
    }
}
