<?php

namespace App\Http\Controllers;

use App\Http\Requests\CountryRequest;
use App\Models\Country;

class CountryController extends Controller
{
    //
    public function index()
    {
        $country = Country::all();
        return($country);
    }
    public function store(CountryRequest $request)
    {
        $country = new Country($request->input());
        // dd($country);
        $country->saveOrFail();
        
        return($country);

    }
}
