<?php

namespace App\Http\Controllers;

use App\Http\Requests\LangueajeRequestest;
use App\Models\Language;

class LanguageController extends Controller
{
    //
    public function index()
    {
        $lenguages = Language::all();
        return($lenguages);
    }
    public function store(LangueajeRequestest $request)
    {
        
        $lenguage = new Language($request->input());
        $lenguage->saveOrFail();
        
        return($lenguage);

    }
}
