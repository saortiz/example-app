<?php

namespace App\Imports;

use App\Jobs\ProcessFilms;
use App\Models\Film;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

use function Ramsey\Uuid\v1;

class FilmsImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        // dd($rows);
        foreach ($rows as $row) 
        {

            $date = Date::excelToDateTimeObject($row['launch']);
            $dateFor = Carbon::parse($date)->format('Y-m-d');
            
            if($row['title'] === null){
                return;
            }
            $data =[
                'title' => $row['title'],
                'director' => $row['director'],
                'launch' => $dateFor,
                'gender' => $row['gender'],
                'duration' => $row['duration'],
                'synopsis' => $row['synopsis'],
                'actors' => $row['actors'],
                'country_id' => $row['country_id'],
                'classification' => $row['classification'],
                'score' => $row['score'],
                'production' => $row['production'],
                'format' => $row['format'],
                'language_id' => $row['language_id']
            ];
            ProcessFilms::dispatch($data);
        }
    }

}
