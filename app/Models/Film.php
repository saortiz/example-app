<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    protected $table = 'films';

    protected $fillable = [
        'title',
        'director',
        'launch',
        'gender',
        'duration',
        'synopsis',
        'actors',
        'country_id',
        'classification',
        'score',
        'production',
        'format',
        'language_id'
    ];
}
