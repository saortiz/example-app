<?php

namespace App\Jobs;

use App\Models\LogData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class ProcessFilms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $data;
    /**
     * Create a new job instance.
     */
    public function __construct($data)
    {
        $this->data= $data;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        // dd($this->data);
        try {
            $response = Http::post('http://192.168.56.56/api/film/validate', $this->data);
            // dd($response->body());
            // dd($response->status());
            $log = new LogData();

            $log->data = json_encode($this->data);
            if($response->status() == 200){
                $log->status = true;
            }else{
                $log->status = false;
            }
            $log->saveOrFail();
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
    }
}
