<?php

use App\Models\Country;
use App\Models\Language;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('films', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('director');
            $table->date('launch');
            $table->string('gender');
            $table->string('duration');
            $table->string('synopsis');
            $table->string('actors');
            $table->foreignIdFor(Country::class);
            $table->enum('classification',['G', 'PG','M','MA15+','R18+']);
            $table->string('score');
            $table->string('production');
            $table->enum('format', ['Full-frame','Academico', 'Panoramico europeo', 'Panoramico americano', 'Cinerama', 'Cinemascope', 'Vistavision', 'IMAX', 'Digital 3D']);
            $table->foreignIdFor(Language::class);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('films');
    }
};
